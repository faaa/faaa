﻿// Released to the public domain. Use, modify and relicense at will.

using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using WesternLib;
using System.Windows.Forms;
using System.IO;

namespace Western
{
    class Game : GameWindow
    {
        World _world;
        private IGameStateHandler _state;
        private Dictionary<GameState, IGameStateHandler> _states = new Dictionary<GameState,IGameStateHandler>(); 
        public bool Developer => true;


        public World World { get { return _world; } }
        
        public Game()
            : base(800,600)
        {
            _world = new World();
            Title = "Mostacho";
            _world.Options.DrawCollision = false;
            _world.Options.DrawHitbox = false;
            _world.Options.DrawTriggers = false;

            Console.WriteLine(GL.GetString(StringName.Version));
            VSync = VSyncMode.On;
            Mouse.ButtonDown += Mouse_ButtonDown;
            Keyboard.KeyDown += Keyboard_KeyDown;
            WindowBorder = OpenTK.WindowBorder.Resizable;
        }
        

 

        void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            GameState newState = _state.HandleKeypress(e);
            _state = _states[newState];
        }

        void Mouse_ButtonDown(object sender, MouseButtonEventArgs e)
        {
            Vector2 mousePos = World.Camera.ToTileCoordinates(Mouse.X, Mouse.Y);
            GameState newState = _state.HandleMousePress(e);
            _state = _states[newState];

        }
        

        private void ChangeLevel()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Level files|*.xml";
            dialog.InitialDirectory = Path.Combine(Directory.GetCurrentDirectory(), "levels");
            if (dialog.ShowDialog() == DialogResult.OK)
                _world.ChangeLevel(dialog.FileName);
            else
                _world.ChangeLevel("default.xml");
        }

        /// <summary>Load resources here.</summary>
        /// <param name="e">Not used.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            _world.Init();
            InGameState inGameState = new InGameState(World, Exit);
            _states.Add(GameState.Game, inGameState);
            _states.Add(GameState.Dialog, new InDialogState(_world.GraphicsEngine.Dialog));
            if(Developer)
                inGameState.EnableDeveloperMode(ChangeLevel);
            _state = _states[_world.State];
            _world.OnStateChanged += s => _state = _states[_world.State];

            _world.ChangeLevel("town.xml");

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

            System.Threading.Thread.Sleep(1);
        }


        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            _world.Camera.SetScreenDimensions(ClientRectangle.Width, ClientRectangle.Height);
        }



        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            
            Vector2 mousePos = World.Camera.ToTileCoordinates(Mouse.X, Mouse.Y);
            _state.UpdateFrame(e, Mouse);


            if (e.Time > 0.15)
                _world.Update(0.15);
            else
                _world.Update(e.Time);
            _world.Camera.LookAt(_world.Player.Position);
        }
        
        /// <summary>
        /// Called when it is time to render the next frame. Add your rendering code here.
        /// </summary>
        /// <param name="e">Contains timing information.</param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            _world.Draw(e.Time);
            SwapBuffers();
        }

        [STAThread]
        static void Main()
        {
            using (Game game = new Game())
            {
                try
                {
                    game.Run(50, 30);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Console.ReadKey();
                }
            }
        }
    }
}