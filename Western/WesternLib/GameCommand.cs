﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WesternLib
{
    public interface IGameCommand
    {
        void Execute();
    }

    public class ActionCommand : IGameCommand
    {
        private readonly Action _action;
        public ActionCommand(Action action)
        {
            _action = action;
        }

        public static ActionCommand FromFunc<T>(Func<T> func)
        {
            return new ActionCommand(() => func());
        }

        public void Execute()
        {
            _action();
        }
    }

    public class HumanCommand : IGameCommand
    {
        private readonly Human _pawn;
        private readonly Action<Human> _action;

        public HumanCommand(Human pawn, Action<Human> action)
        {
            _pawn = pawn;
            _action = action;
        }

        public void Execute()
        {
            _action(_pawn);
        }
    }
}
