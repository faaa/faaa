﻿using System;

namespace WesternLib
{
    internal static class GameConstants
    {
        public const int TILE_SIZE = 32;
        public const float TILE_SIZEf = TILE_SIZE;
        public const float MOVE_ACCEL = 0.4f;
        public const float MOVE_ACCEL_SQUARED = MOVE_ACCEL * MOVE_ACCEL;
        public const float STRAFE_ACCEL = 0.3f;
        public const float MOVE_DAMP = 0.1f; // percentage of ACCEL 
        public const float BULLET_SPEED = 1.7f;
        public const int TICKS_PER_SECOND = 10;
        public const float ANIM_SPEED_MOD = 0.1f;

        public const float HUMAN_WIDTH = 0.7f;
        public const float HUMAN_HEIGHT = 1.8f;
        public const float ENEMY_SPEED = 0.2f;

        public const float TOMAHAWK_BREAK_FACTOR = 0.98f;
    }

    public static class MathConstants
    {
        public static readonly float SQRT2 = (float)Math.Sqrt(2);
       
    }
}
