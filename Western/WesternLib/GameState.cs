﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Input;

namespace WesternLib
{
    public enum GameState
    {
        Game,
        Dialog,
        Editor,
        Loading
    };
    public interface IGameStateHandler
    {
        GameState HandleKeypress(KeyboardKeyEventArgs e);
        GameState HandleMousePress(MouseButtonEventArgs e);
        GameState UpdateFrame(FrameEventArgs e, MouseDevice mouse);
    }

    public class InGameState : IGameStateHandler
    {
        private World _world;
        private Dictionary<Key, IGameCommand> _keyCommands;
        

        public InGameState(World world, Action exit)
        {
            _world = world;

            _keyCommands = new Dictionary<Key, IGameCommand>
                {
                    {Key.Space,  new HumanCommand(_world.Player, p => p.Shoot())},
                    {Key.P, new HumanCommand(_world.Player, p => p.Equipped.Reload()) },
                    {Key.Escape, new ActionCommand(exit)}

                };
        }

        public void EnableDeveloperMode(Action changeLevel)
        {
            Player player = _world.Player;
            RenderingOptions options = _world.Options;
            var devShortcuts = new Dictionary<Key, IGameCommand>
            {
                { Key.F1, new ActionCommand(options.DrawCollision.Toggle)},
                {Key.F2, new ActionCommand(options.DrawTriggers.Toggle)},
                {Key.F3, new ActionCommand(options.DrawHitbox.Toggle)},
                {Key.F5, new HumanCommand(player, p => p.Health = 1f)},
                {Key.L, new ActionCommand(changeLevel)},
                {Key.F11, new HumanCommand(player, p => p.MaxVelocity -= 0.1f)},
                {Key.F12, new HumanCommand(player, p => p.MaxVelocity += 0.1f)}
            };
            _keyCommands.Add(devShortcuts);
        }

        public GameState HandleKeypress(KeyboardKeyEventArgs e)
        {
            if (_keyCommands.ContainsKey(e.Key))
                _keyCommands[e.Key].Execute();
            return GameState.Game;
        }

        public GameState UpdateFrame(FrameEventArgs e, MouseDevice mouse)
        {
            Vector2 mousePos = _world.Camera.ToTileCoordinates(mouse.X, mouse.Y);
            _world.Player.LookAt(mousePos);
            if (mouse[MouseButton.Left])
                _world.Player.MoveTowards(mousePos);
            return GameState.Game;
        }

        public GameState HandleMousePress(MouseButtonEventArgs e)
        {
            if (e.Button == MouseButton.Right)
                _world.Player.Shoot();
            return GameState.Game;
        }
    }

    public class InDialogState : IGameStateHandler
    {
        private DialogUI _dialog;
        public InDialogState(DialogUI dialog)
        {
            _dialog = dialog;

        }
        public GameState HandleKeypress(KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                if (!_dialog.MoveNext())
                    return GameState.Game;
            }
            else if (e.Key == Key.C)
            {
                if (_dialog.Abort())
                    return GameState.Game;
            }

            return GameState.Dialog;
        }

        public GameState HandleMousePress(MouseButtonEventArgs e)
        {
            if (e.Button == MouseButton.Left)
                if (_dialog.Click(e.X, e.Y))
                    return GameState.Game;
            return GameState.Dialog;
        }

        public GameState UpdateFrame(FrameEventArgs e, MouseDevice mouse)
        {
            //if (mouse[MouseButton.Left])
            //    _dialog.Click(mouse.X, mouse.Y);
            return GameState.Dialog;
        }
    }
}
